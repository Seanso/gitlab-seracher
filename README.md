GitLab Searcher
===============
Use this tool to search for a pattern in all your repositories and branches.
This script can be used to pull all repositories from your gitlab group and then serach through all your branches for a specfic pattern.

The script get a file with all your gitlab repositories urls, you can use the following command to create it:
```
curl --header "PRIVATE-TOKEN: <token>" "https://gitlab.com/api/v4/groups/<group_id>/projects" | jq -r .[].http_url_to_repo > repos_urls
```

Clone and look for a string in a gitlab repository group (including all branch)

```
Syntax: ./dummy.sh [-c|-s|-h|]
options:
-c / --clone      Clone repositories from a gitlab group.
-h / --help       Print this Help.
-s / --search     Search for a string in all repositories and branches.
```
