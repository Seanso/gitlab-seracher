#!/bin/bash
CLONE=0
SEARCH=0
#echo $GIT
Help ()
{
   # Display Help
   echo "Clone and look for a string in a gitlab repository group (including all branch)"
   echo
   echo "Syntax: ./dummy.sh [-c|-s|-h|]"
   echo "options:"
   echo "-c / --clone      Clone repositories from a gitlab group."
   echo "-h / --help       Print this Help."
   echo "-s / --search     Search for a string in all repositories and branches."
   echo
}

for arg in "$@"
do
    case $arg in
        -c|--clone)
        CLONE=1
        shift # Remove --initialize from processing
        ;;
        -s|--search)
        SEARCH=1
        shift # Remove --initialize from processing
        ;;
        -h|--help)
        Help
        shift # Remove argument name from processing
        shift # Remove argument value from processing
        ;;
        -p|--pattern)
        PATTERN="$2"
        shift # Remove argument name from processing
        shift # Remove argument value from processing
        ;;
        *)
	Help
        shift # Remove generic argument from processing
        ;;
    esac
done
echo $PATTERN
if [[ $CLONE -eq 1 ]]; then
   while IFS= read -r line
   do 
      git clone $line
   done < repos_urls
else 
	echo "INFO: Cloning is disabled, use -c / --clone to clone repositories"
fi

if [[ $SEARCH -eq 1 ]] ; then
   for i in `cat repos_urls | awk -F "/" '{print $5}' | awk -F . '{print $1}'` 
   do 
      cd $i
      echo "===================================================== Moved to $i Repository ======================================================"
      for i in $(git branch --list --all | sed 's/^  //' | sed 's/* (HEAD detached at //' | sed 's/)$//')
      do	
         git checkout $i 2> /dev/null 1>/dev/null
	 echo "======= Branch $i"
	 #grep -Ri bitbucket . | grep -i git | grep -v .gitignore
	 grep -Ri $PATTERN .
      done
      cd ..
   done
else
   echo "INFO: Searching is disabled, use -s / --search to search for a string in all repositories"	
fi
